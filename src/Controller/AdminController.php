<?php

namespace App\Controller;

use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin/tasks', name: 'app_admin_tasks')]
    public function getTasksWithoutUsers(TaskRepository $taskRepository): Response
    {

        $tasks = $taskRepository->findBy(['user' => null]);
        return $this->render('admin/list.html.twig', [
            'tasks' => $tasks,
        ]);
    }

    #[Route('/admin/tasks/{id}/delete', name: 'app_admin_tasks_delete')]
    public function deleteTasksWithoutUsers($id, TaskRepository $taskRepository, EntityManagerInterface $em, UserRepository $userRepository)
    {
        $task = $taskRepository->findOneBy(['id' => $id]);

        $user = $this->getUser();
        $user = $user->getUserIdentifier();
        $user = $userRepository->findOneBy(['email' => $user]);
        $userRole = $user->getRoles();

        //Check if ROLE_ADMIN in roles
        if (!in_array('ROLE_ADMIN', $userRole)) {
            $this->addFlash('error', 'Vous devez être administrateur pour supprimer une tâche.');
            return $this->redirectToRoute('task_list');
        }

        $em->remove($task);
        $em->flush();

        $this->addFlash('success', 'La tâche a bien été supprimée.');

        return $this->redirectToRoute('app_admin_tasks');
    }
}
