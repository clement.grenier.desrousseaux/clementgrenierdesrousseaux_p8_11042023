<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController
{
    /**
     * @Route("/tasks", name="task_list")
     */
    public function getTasks(TaskRepository $taskRepository, UserRepository $userRepository)
    {
        $user = $this->getUser();

        if($user == null){
            $this->addFlash('error', 'Vous devez être connecté pour voir la liste des tâches.');
            return $this->redirectToRoute('task_list');
        }

        $user = $user->getUserIdentifier();
        $user = $userRepository->findOneBy(['email' => $user]);

        $tasks = $taskRepository->findBy(['user' => $user]);


        return $this->render('task/list.html.twig', [
            'tasks' => $tasks,
        ]);
    }

    /**
     * @Route("/tasks/create", name="task_create")
     */
    public function createTask(Request $request, EntityManagerInterface $em, UserRepository $userRepository)
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);
        $user = $this->getUser();

        if($user == null){
            $this->addFlash('error', 'Vous devez être connecté pour créer une tâche.');
            return $this->redirectToRoute('task_list');
        }

        $user = $user->getUserIdentifier();

        $user = $userRepository->findOneBy(['email' => $user]);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task->setUser($user);
            $em->persist($task);
            $em->flush();

            $this->addFlash('success', 'La tâche a été bien été ajoutée.');

            return $this->redirectToRoute('task_list');
        }

        return $this->render('task/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/tasks/{taskIdentifier}/edit", name="task_edit")
     */
    public function modifyTask($taskIdentifier, Request $request, TaskRepository $taskRepository, EntityManagerInterface $em)
    {

        $task = $taskRepository->findOneBy(['id' => $taskIdentifier]);

        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'La tâche a bien été modifiée.');

            return $this->redirectToRoute('task_list');
        }

        return $this->render('task/edit.html.twig', [
            'form' => $form->createView(),
            'task' => $task,
        ]);
    }

    /**
     * @Route("/tasks/{taskIdentifier}/toggle", name="task_toggle")
     */
    public function toggleTask($taskIdentifier, TaskRepository $taskRepository, EntityManagerInterface $em)
    {
        $task = $taskRepository->findOneBy(['id' => $taskIdentifier]);
        $task->toggle(!$task->isDone());
        $em->flush();

        if ($task->isDone()) {
            $this->addFlash('success', sprintf('La tâche "%s" a bien été marquée comme faite.', $task->getTitle()));
        } else {
            $this->addFlash('success', sprintf('La tâche "%s" a bien été marquée comme à faire.', $task->getTitle()));
        }

        return $this->redirectToRoute('task_list');
    }

    /**
     * @Route("/tasks/{taskIdentifier}/delete", name="task_delete")
     */
    public function deleteTask($taskIdentifier, TaskRepository $taskRepository, EntityManagerInterface $em, UserRepository $userRepository)
    {
        $task = $taskRepository->findOneBy(['id' => $taskIdentifier]);

        $user = $this->getUser();
        $user = $user->getUserIdentifier();
        $user = $userRepository->findOneBy(['email' => $user]);

        if($task->getUser() !== $user){
            $this->addFlash('error', 'Vous ne pouvez pas supprimer une tâche qui ne vous appartient pas.');
            return $this->redirectToRoute('task_list');
        }

        $em->remove($task);
        $em->flush();

        $this->addFlash('success', 'La tâche a bien été supprimée.');

        return $this->redirectToRoute('task_list');
    }
}
