<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager): void
    {
        $userAdmin = new User();
        $userAdmin->setEmail('clement@test.admin');
        $userAdmin->setPassword($userAdmin->hashPassword('test1234'));
        $userAdmin->setRoles(['ROLE_ADMIN']);

        $manager->persist($userAdmin);

        $userUser1 = new User();
        $userUser1->setEmail('david@test.user');
        $userUser1->setPassword($userUser1->hashPassword('test1234'));
        $userUser1->setRoles(['ROLE_USER']);

        $manager->persist($userUser1);

        for ($i = 0; $i < 15; $i++) {
            $task = new Task();
            $task->setTitle('Tâche ' . $i);
            $task->setContent('Description de la tâche ' . $i);
            if ($i < 5) {
                $task->setUser($userUser1);
            } else if ($i < 10) {
                $task->setUser($userAdmin);
            } else {
                $task->setUser(null);
            }

            $manager->persist($task);
        }





        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['users'];
    }
}
