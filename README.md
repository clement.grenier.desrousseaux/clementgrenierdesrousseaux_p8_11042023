# To Do List App

An app where you can create tasks, complete them or delete them.

---

# Installation <ins>without Docker</ins> <a id="paragraph1">

### Clone the project and install dependencies 

```bash
git clone https://gitlab.com/clement.grenier.desrousseaux/clementgrenierdesrousseaux_p8_11042023.git
cd clementgrenierdesrousseaux_p8_11042023
composer install
touch .env.local
```

Add in .env.local file :

```
APP_ENV=dev

###> DATABASE CREDENTIALS ###
DATABASE_URL="mysql://!username!:!password!@localhost:3306/app?serverVersion=8&charset=utf8mb4"
###< DATABASE CREDENTIALS ###
```

### Create the database and load fixtures

```bash
php bin/console doctrine:database:create
php bin/console make:migration
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
symfony serve
```

---

# Installation <ins>with Docker</ins> <a id="paragraph2">

### Clone the project

```bash
git clone https://gitlab.com/clement.grenier.desrousseaux/clementgrenierdesrousseaux_p8_11042023.git
```

### Install dependencies and configure the project
```bash
cd clementgrenierdesrousseaux_p8_11042023
docker-compose up -d
docker exec -it myapp_p8 bash
composer install
touch .env.local
```

Add in .env.local file :

```
DATABASE_URL="mysql://root:password@database:3306/app?serverVersion=8&charset=utf8mb4"

APP_ENV=dev

###> DATABASE CREDENTIALS ###
DATABASE_URL="mysql://root:password@database:3306/app?serverVersion=8&charset=utf8mb4"
###< DATABASE CREDENTIALS ###
```

### Init the database and load fixtures

```bash
symfony console make:migration
symfony console doctrine:migrations:migrate
symfony console doctrine:fixtures:load  
symfony serve
```

Go to ```localhost:9070``` 

NB: if you want to change database credentials or the port, you can do it on the docker-compose.yml file, but don't forget to do ```docker-compose up -d``` to update the container

