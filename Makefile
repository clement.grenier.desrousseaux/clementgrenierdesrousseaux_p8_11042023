SHELL := /bin/bash

MakeMigrations:
	php bin/console make:migration
	php bin/console d:m:m

MakeMigrationsTest:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test

TaskTestFixtures:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test
	symfony console doctrine:fixtures:load --group=users -n --env=test

TaskFixtures:
	symfony console doctrine:database:drop --force || true
	symfony console doctrine:database:create
	symfony console doctrine:migrations:migrate -n
	symfony console doctrine:fixtures:load --group=users -n

CommentsTestFixtures:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test
	symfony console doctrine:fixtures:load --group=users --group=tricks -n --env=test

RegistrationTestFixtures:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test

LoadFixturesTest:
	symfony console doctrine:database:drop --force --env=test || true
	symfony console doctrine:database:create --env=test
	symfony console doctrine:migrations:migrate -n --env=test
	symfony console doctrine:fixtures:load --group=tricks -n --env=test

TestCoverage:
	XDEBUG_MODE=coverage vendor/bin/phpunit --coverage-html test-coverage

ConnectToPHPContainer:
	docker exec -it myapp_p8 bash

Cache:
	symfony console cache:clear

.PHONY: tests
