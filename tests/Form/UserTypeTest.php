<?php

namespace App\Tests\Form;

use App\Form\UserType;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Test\TypeTestCase;

class UserTypeTest extends TypeTestCase
{

    public function testBuildForm()
    {
        $formData = [
            'password' => [
                'first' => 'password123',
                'second' => 'password123',
            ],
            'email' => 'clement@test.test',
        ];

        $form = $this->factory->create(UserType::class);
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($formData['password']['first'], $form->get('password')->getData());
        $this->assertEquals($formData['password']['second'], $form->get('password')->getData());
        $this->assertEquals($formData['email'], $form->get('email')->getData());


    }
}
