<?php

namespace App\Tests\Form;

use App\Entity\User;
use App\Form\RegistrationFormType;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Test\TypeTestCase;

class RegistrationFormTypeTest extends TypeTestCase
{
//    public function testSubmitValidData()
//    {
//        $formData = [
//            'email' => 'test@example.com',
//            'plainPassword' => 'password123',
//        ];
//
//        $form = $this->factory->create(RegistrationFormType::class);
//        $form->submit($formData);
//
//        $this->assertTrue($form->isSynchronized());
//        $this->assertEquals($formData['email'], $form->get('email')->getData());
//        $this->assertEquals($formData['plainPassword'], $form->get('plainPassword')->getData());
//
//
//    }

    public function testSubmitValidData2()
    {
        $formData = [
            'email' => 'cer@ger.com',
            'plainPassword' => 'password123',
        ];

        $user = new User();
        $user->setEmail('cer@ger.com');
        $user->setPassword('password123');

        $form = $this->factory->create(RegistrationFormType::class, $user);
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($formData['email'], $form->get('email')->getData());
        $this->assertEquals($formData['plainPassword'], $form->get('plainPassword')->getData());

    }
}
