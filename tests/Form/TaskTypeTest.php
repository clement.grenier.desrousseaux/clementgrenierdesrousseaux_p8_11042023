<?php

namespace App\Tests\Form;

use App\Form\TaskType;
use Symfony\Component\Form\Test\TypeTestCase;

class TaskTypeTest extends TypeTestCase
{

    public function testSubmitValidData()
    {
        $formData = [
            'title' => 'Task Title',
            'content' => 'Task Content',
        ];

        $form = $this->factory->create(TaskType::class);
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($formData['title'], $form->get('title')->getData());
        $this->assertEquals($formData['content'], $form->get('content')->getData());
    }
}
