<?php

namespace App\Tests\Entity;

use App\Entity\Task;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * @test
     */
    public function addUserNameToUser()
    {
        $user = new User();
        $user->setEmail('test@test.testtest');
        $this->assertEquals('test@test.testtest', $user->getUsername());
    }

    /**
     * @test
     */
    public function addEmailToUser()
    {
        $user = new User();
        $user->setEmail('test@test.test');
        $this->assertEquals('test@test.test', $user->getEmail());
    }

    /**
     * @test
     */
    public function addPasswordToUser()
    {
        $user = new User();
        $user->setPassword('test');
        $this->assertEquals('test', $user->getPassword());
    }

    /**
     * @test
     */
    public function addSaltToUser()
    {
        $user = new User();
        $this->assertEquals(null, $user->getSalt());
    }

    /**
     * @test
     */
    public function addRolesToUser()
    {
        $user = new User();
        $this->assertEquals(['ROLE_USER'], $user->getRoles());
    }

    /**
     * @test
     */
    public function getRolesFromUser() {
        $user = new User();
        $user->setRoles(['ROLE_ADMIN']);
        $this->assertEquals(['ROLE_ADMIN', 'ROLE_USER'], $user->getRoles());
    }

    /**
     * @test
     */
    public function getTaks()
    {
        $user = new User();
        $task = new Task();
        $user->addTask($task);
        $this->assertEquals($task, $user->getTasks()[0]);
    }

    /**
     * @test
     */
    public function removeTask() {
        $user = new User();
        $task = new Task();
        $user->addTask($task);
        $user->removeTask($task);
        $this->assertEquals(null, $user->getTasks()[0]);
    }

    /**
     * @test
     */
    public function hashPassword()
    {
        $user = new User();
        $password_unhash = 'test';
        $password_hash = $user->hashPassword($password_unhash);
        $this->assertNotEquals($password_unhash, $password_hash);
    }
}
