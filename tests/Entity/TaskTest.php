<?php

namespace App\Tests\Entity;

use App\Entity\Task;
use PHPUnit\Framework\TestCase;

class TaskTest extends TestCase
{
    /**
     * @test
     */
    public function getCreatedAt()
    {
        $task = new Task();
        $this->assertInstanceOf(\DateTime::class, $task->getCreatedAt());
    }

    /**
     * @test
     */
    public function setCreatedAt()
    {
        $task = new Task();
        $task->setCreatedAt(new \DateTime('2018-01-01'));
        $this->assertEquals(new \DateTime('2018-01-01'), $task->getCreatedAt());
    }

    /**
     * @test
     */
    public function getTitle()
    {
        $task = new Task();
        $task->setTitle('Test title');
        $this->assertEquals('Test title', $task->getTitle());
    }

    /**
     * @test
     */
    public function getContent()
    {
        $task = new Task();
        $task->setContent('Test content');
        $this->assertEquals('Test content', $task->getContent());
    }

    /**
     * @test
     */
    public function isDone() {
        $task = new Task();
        $this->assertFalse($task->isDone());
    }

    /**
     * @test
     */
    public function toggleIsDone() {
        $task = new Task();
        $task->toggle(true);
        $this->assertTrue($task->isDone());
    }

}
