<?php

namespace App\Tests\Security;

use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AccessRightsTest extends WebTestCase
{
    public static function setUpBeforeClass(): void
    {
        shell_exec('make TaskTestFixtures');
    }

    /**
     * @test
     * @throws Exception
     */
    public function AsAdminICanGoOnUsersPageManager()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('clement@test.admin');
        $client->loginUser($testUser);


        $client->request('GET', '/users');

        $this->assertResponseIsSuccessful();
    }

    /**
     * @test
     * @throws Exception
     */
    public function AsUserICanNotGoOnUsersPageManager()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');
        $client->loginUser($testUser);


        $client->request('GET', '/users');

        $this->assertResponseStatusCodeSame(403);
    }
}
