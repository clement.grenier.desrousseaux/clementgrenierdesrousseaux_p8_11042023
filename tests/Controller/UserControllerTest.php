<?php

namespace App\Tests\Controller;

use App\Controller\UserController;
use App\Repository\UserRepository;
use PHPUnit\Framework\TestCase;
use Qameta\Allure\Attribute\Description;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use function PHPUnit\Framework\assertNotNull;

#[Description('Test for UserController')]
class UserControllerTest extends WebTestCase
{

    public static function setUpBeforeClass(): void
    {
        shell_exec('make TaskTestFixtures');
    }

    public function testListAction()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('clement@test.admin');
        $client->loginUser($testUser);

        $client->request('GET', '/users');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

    }

    public function testEditAction()
    {

        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('clement@test.admin');
        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/users/2/edit');

        $form = $crawler->selectButton('Modifier')->form();

        $form['user_edit[email]'] = 'david@test.user';
        $form['user_edit[roles][0]'] = true;

        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect('/users'));

        $client->followRedirect();

        $this->assertResponseIsSuccessful();

        $userModified = $userRepository->find(2);
        self::assertContains('ROLE_ADMIN', $userModified->getRoles());

    }



    public function testCreateAction()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('clement@test.admin');
        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/users/create');

        $form = $crawler->selectButton('Ajouter')->form();

        $form['user[email]'] = 'michel@test.user';
        $form['user[password][first]'] = 'test1234';
        $form['user[password][second]'] = 'test1234';

        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect('/users'));

        $client->followRedirect();

        $this->assertResponseIsSuccessful();

        $userModified = $userRepository->find(3);
        $this->assertNotNull($userModified);
    }
}
