<?php

namespace App\Tests\Controller;

use App\Controller\SecurityController;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{

    public function testLogout()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');
        $client->loginUser($testUser);

        $client->request('GET', '/logout');
        $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testLogin()
    {
        $client = static::createClient();


        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Sign in')->form();

        $form['email'] = 'david@test.user';
        $form['password'] = 'test1234';

        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect('/tasks'));

        $client->followRedirect();

        $this->assertResponseIsSuccessful();
    }
}
