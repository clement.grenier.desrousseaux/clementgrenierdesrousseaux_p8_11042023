<?php

namespace App\Tests\Controller;

use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskControllerTest extends WebTestCase
{
    public static function setUpBeforeClass(): void
    {
        shell_exec('make TaskTestFixtures');
    }

    /**
     * @test
     * @throws Exception
     */
    public function createAction()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');
        $client->loginUser($testUser);


        $crawler = $client->request('GET', '/tasks/create');

        $form = $crawler->selectButton('Ajouter')->form();

        $form['task[title]'] = 'Nouvelle tâche test';
        $form['task[content]'] = 'Description de la tâche test';

        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect('/tasks'));

        $client->followRedirect();

        $this->assertResponseIsSuccessful();

        $taskRepository = static::getContainer()->get(TaskRepository::class);
        $task = $taskRepository->find(16);

        $this->assertSame('Nouvelle tâche test', $task->getTitle());
        $this->assertSame('Description de la tâche test', $task->getContent());

    }

    public function testEditAction()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');
        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/tasks/16/edit');

        $form = $crawler->selectButton('Modifier')->form();

        $form['task[title]'] = 'Nouvelle tâche modifiée';
        $form['task[content]'] = 'Description de la tâche modifiée';

        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirect('/tasks'));

        $client->followRedirect();

        $this->assertResponseIsSuccessful();

        $taskRepository = static::getContainer()->get(TaskRepository::class);
        $task = $taskRepository->find(16);

        $this->assertSame('Nouvelle tâche modifiée', $task->getTitle());
        $this->assertSame('Description de la tâche modifiée', $task->getContent());
    }

    /**
     * @throws Exception
     */
    public function testToggleTaskAction()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');
        $client->loginUser($testUser);

        $taskRepository = static::getContainer()->get(TaskRepository::class);
        $task = $taskRepository->find(16);

        $this->assertFalse($task->isDone());

        $client->request('GET', '/tasks/16/toggle');

        $task = $taskRepository->find(16);

        $this->assertTrue($task->isDone());
    }

    /**
     * @test
     */
    public function asUserICanOnlyDeleteMyTasks()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail('david@test.user');
        $client->loginUser($testUser);

        $taskRepository = static::getContainer()->get(TaskRepository::class);
        $task = $taskRepository->find(2);

        $this->assertNotNull($task);

        $client->request('GET', '/tasks/2/delete');

        $task = $taskRepository->find(2);

        $this->assertNull($task);
    }
}
