# To Do List App

### How to contribute to the project

### 1. First step : install the project

Check the [README.md](README.md) file for the installation (it will vary if you are using Docker or not)

### 2. Choose an issue

You can go check the open issues on the gitlab project : https://gitlab.com/clement.grenier.desrousseaux/clementgrenierdesrousseaux_p8_11042023/-/boards/5539670
If you want to add something but the issue doesn't exist, you can create the issue and add different labels

### 3. Merge Request

You have to go on the issue you choose, and create a merge request. It will automatically create a new branch (don't forget to do a ``` git pull ``` to get the new branch on your local project)

### 4. Tests

If you add new features, check if existent tests are OK, and create new test to check if your new features works !

You can use the Test Driven Development method, this is this method which is used in this project.

### 5. Final check

When you have completed the issue, you can "mark as ready" your merge request, and if the pipeline are OK and the reviewer doesn't find errors, the merge request will be accepted. If there is some probleme, it will be write in the comment section of the merge request.

---
